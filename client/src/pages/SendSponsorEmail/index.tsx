import './styles.scss'

import { sendEmailToAllSponsors } from "../../firebase/functions"
import { useState } from 'react'

export default function index() {
    const [password, setPassword] = useState<string>('')
    const [isConfirmed, setIsConfirmed] = useState(false)
    const [isSent, setIsSent] = useState(false)

    function handleConfirmPassword(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault()
        if (password === import.meta.env.VITE_EJS_PUBLIC_KEY) {
            setIsConfirmed(true)
        } else {
            setPassword('')
        }
    }

    function handleSubmit() {
        sendEmailToAllSponsors()
        setIsSent(true)
    }

    return (
        <div className="confirmation-page">
            {isSent ? (
                <h1>Emails enviados!</h1>
            ) : isConfirmed ? (
                <button onClick={handleSubmit} className="btn btn-primary">Enviar email de confirmação de apadrinhamento</button>
            ) : (
                <form onSubmit={handleConfirmPassword}>
                    <label>
                        Senha
                        <input
                            value={password}
                            onChange={e => setPassword(e.target.value)}
                            type="password"
                        />
                    </label>
                    <button type='submit' className='btn btn-primary'>Confirmar</button>
                </form>
            )}
        </div>
    )
}
