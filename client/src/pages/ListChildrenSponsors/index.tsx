import { getChildrenSponsorCSV } from "../../firebase/functions"
import { useEffect } from "react"
import useHasMounted from "../../hooks/useHasMounted"

export default function ListChildrenSponsors() {
    const hasMounted = useHasMounted()

    useEffect(() => {
        async function getCsv() {
            const csv = await getChildrenSponsorCSV()
            const downloadLink = document.createElement("a");
            const blob = new Blob(["\ufeff", csv]);
            const url = URL.createObjectURL(blob);
            downloadLink.href = url;
            downloadLink.download = "planilha-criancas-padrinhos.csv";

            document.body.appendChild(downloadLink);
            downloadLink.click();
            document.body.removeChild(downloadLink);
        }
        if (hasMounted) {
            getCsv()
        }
    }, [hasMounted])


    return (<></>)
}
