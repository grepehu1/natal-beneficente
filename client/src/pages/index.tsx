import MainPage from './MainPage'
import RedirectAdmin from './RedirectAdmin'
import ListChildrenSponsors from './ListChildrenSponsors'
import SendSponsorEmail from './SendSponsorEmail'

export {
	MainPage,
	RedirectAdmin,
	ListChildrenSponsors,
	SendSponsorEmail,
}