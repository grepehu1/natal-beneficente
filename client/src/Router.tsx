import {
	Routes,
	Route
} from 'react-router-dom'

import {
	MainPage,
	RedirectAdmin,
	ListChildrenSponsors,
	SendSponsorEmail,
} from './pages'

import ScrollToTop from './utils/ScrollToTop'

export default function Router() {
	return (
		<>
			<ScrollToTop />
			<Routes>
				<Route path="/admin" element={<RedirectAdmin />} />
				<Route path="/planilha" element={<ListChildrenSponsors />} />
				<Route path="/email-padrinhos" element={<SendSponsorEmail />} />
				<Route path="/:id" element={<MainPage />} />
				<Route path="/" element={<MainPage />} />
			</Routes>
		</>
	)
}
